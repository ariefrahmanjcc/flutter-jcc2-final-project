# eujobs

A Job Aggregator for European

## Feature

- Firebase
  + Authetication
  + Cloud Storage: for uploading profile picture
- http
- getx
- flutter_html
- camera
- filter result based on job_types
- search result based on description

## API
doc: https://arbeitnow.com/api/job-board-api

## APK File
link: https://drive.google.com/file/d/1x_HaUh5Zw7a1b_B01Z5MkUemfhiABRVL/view?usp=sharing
