import 'package:eujobs/pages/auth/SplashScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'auth/LoginScreen.dart';
import 'package:eujobs/routes/index.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Splash();
        }
        if (snapshot.hasData) {
          //snapshot.
          return Home();
        } else {
          return Login();
        }
      },
    );
  }
}
