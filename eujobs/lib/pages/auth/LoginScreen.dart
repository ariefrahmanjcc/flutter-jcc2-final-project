import 'dart:ui';

import 'package:eujobs/routes/route_name.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  loginSubmit() async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: _emailController.text, password: _passwordController.text);
    } catch (e) {
      print(e);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 40),
              child: Text(
                "Login",
                style: TextStyle(
                    color: Color(0xffBC32FD),
                    fontWeight: FontWeight.w500,
                    fontSize: 30),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Image.asset(
                "lib/assets/mainlogo.png",
                height: 100,
                width: 100,
              ),
            ),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  cursorColor: Color(0xffBC32FD),
                  controller: _emailController,
                  decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffBC32FD), width: 2),
                      ),
                      border: OutlineInputBorder(),
                      labelText: "Email",
                      floatingLabelStyle: TextStyle(color: Color(0xffBC32FD))),
                )),
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  obscureText: true,
                  controller: _passwordController,
                  decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xffBC32FD), width: 2),
                      ),
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      floatingLabelStyle: TextStyle(color: Color(0xffBC32FD))),
                )),
            Container(
                height: 80,
                padding: EdgeInsets.all(10),
                child: ElevatedButton(
                    style: raisedButtonStyle,
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      loginSubmit();
                    })),
            Padding(
                padding: EdgeInsets.only(top: 10),
                child: Center(child: Text("Don't Have Have Account?"))),
            TextButton(
              onPressed: () => Get.toNamed(AuthRouteName.Register),
              child: Text(
                "Register Here",
                style: TextStyle(color: Color(0xffBC32FD)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  //textStyle: TextStyle(color: Colors.white),
  primary: Color(0xffBC32FD),
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
