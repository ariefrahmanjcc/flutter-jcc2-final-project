import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('lib/assets/mainlogo.png'),
          SizedBox(
            height: 10,
          ),
          CircularProgressIndicator(
            color: Color(0xffBC32FD),
          ),
          SizedBox(
            height: 10,
          ),
          Text("Loading...")
        ],
      )),
    );
  }
}
