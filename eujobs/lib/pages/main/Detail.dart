import 'package:eujobs/model/model.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_html/flutter_html.dart';

class Details extends StatelessWidget {
  const Details({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Data item = Get.arguments;
    final created = DateTime.fromMillisecondsSinceEpoch(item.created_at * 1000);
    final diff = timeago.format(created);

    //print(item);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffBC32FD),
          title: const Text('Details'),
        ),
        //drawer: const DrawerScreen(),
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Text(
                item.title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              SizedBox(height: 10),
              Text(
                '--- ${item.company_name}',
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
              Divider(),
              Center(
                  child: Text(
                diff,
              )),
              Divider(),
              Text(
                'Job Types',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30,
                child: ListView.builder(
                  itemCount: item.job_types.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(children: [
                      Text(item.job_types[index]),
                      index == item.job_types.length - 1 ? Text('') : Text(', ')
                    ]);
                  },
                ),
              ),
              SizedBox(height: 5),
              Text(
                'Tags',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30,
                child: ListView.builder(
                  itemCount: item.tags.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Row(children: [
                      Text(item.tags[index]),
                      index == item.tags.length - 1 ? Text('') : Text(', ')
                    ]);
                  },
                ),
              ),
              SizedBox(height: 5),
              Text(
                'Remote',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              item.remote ? Text('Yes') : Text('No'),
              SizedBox(height: 10),
              Text(
                'Description',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              Html(
                data: item.description,
              ),
              Text(
                'Url',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
              Text(item.url)
            ],
          ),
        )));
  }
}
