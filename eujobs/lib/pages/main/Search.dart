import 'package:eujobs/controller/controller.dart';
import 'package:eujobs/pages/main/Detail.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var keyword = Get.arguments;
    var controller = Get.put(AppController());

    var filtered = controller.getposts
        .where((job) =>
            job.description.toLowerCase().contains(keyword.toLowerCase()))
        .toList();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffBC32FD),
        title: const Text('Search'),
      ),
      //drawer: const DrawerScreen(),
      body: Container(
        padding: const EdgeInsets.all(16),
        color: Colors.white,
        child: Column(
          children: [
            const SizedBox(
              height: 15,
            ),
            // Container(
            //     child: TextFormField(
            //         initialValue: keyword == null ? null : keyword,
            //         onFieldSubmitted: (value) {
            //           Get.to(Search(), arguments: value);
            //         },
            //         cursorColor: Color(0xffBC32FD),
            //         decoration: InputDecoration(
            //             focusedBorder: OutlineInputBorder(
            //               borderSide:
            //                   BorderSide(color: Color(0xffBC32FD), width: 2),
            //             ),
            //             hintText: "Search jobs or position",
            //             border: OutlineInputBorder(
            //                 borderSide: BorderSide(color: Color(0xffBC32FD))),
            //             prefixIcon: Padding(
            //                 padding: EdgeInsets.all(0),
            //                 child: Icon(Icons.search))))),
            // const SizedBox(
            //   height: 10,
            // ),
            Container(
              alignment: Alignment.centerLeft,
              child: const Text(
                "Result",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(child: Obx(() {
              return controller.postloading.value
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : filtered.length == 0
                      ? Center(
                          child: Text('jobs not found'),
                        )
                      : ListView.builder(
                          //shrinkWrap: true,
                          //scrollDirection: Axis.vertical,
                          itemCount: filtered.length,
                          itemBuilder: (context, index) {
                            var item = filtered[index];
                            return GestureDetector(
                                onTap: () {
                                  print("success ${index}");
                                  Get.to(Details(), arguments: item);
                                },
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  padding: EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1, color: Colors.grey),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        item.title,
                                        style: TextStyle(fontSize: 20),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                              child: Text(
                                            item.company_name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          )),
                                          Text("loc: ${item.location}"),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                              child: Container(
                                                  child: Text("types",
                                                      style: TextStyle(
                                                          fontWeight: FontWeight
                                                              .bold)))),
                                          Expanded(
                                              child: Text('tags',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)))
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Flexible(
                                              child: Container(
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount:
                                                    item.job_types.length,
                                                itemBuilder: (c, i) {
                                                  var a = item.job_types;
                                                  return Container(
                                                    child: Text(a[i]),
                                                  );
                                                }),
                                          )),
                                          Flexible(
                                              child: Container(
                                            child: ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: item.tags.length,
                                                itemBuilder: (c, i) {
                                                  var a = item.tags;
                                                  return Container(
                                                    child: Text(
                                                      a[i],
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  );
                                                }),
                                          ))
                                        ],
                                      ),
                                    ],
                                  ),
                                ));
                          });
            })
                // Column(
                //   children: [

                //   ],
                // ),
                ),
          ],
        ),
      ),
    );
  }
}
