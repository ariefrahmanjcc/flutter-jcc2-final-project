import 'package:eujobs/components/drawer.dart';
import 'package:eujobs/controller/controller.dart';
import 'package:eujobs/pages/main/Detail.dart';
import 'package:eujobs/pages/main/Filter.dart';
import 'package:eujobs/pages/main/Search.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(AppController());
    //print(controller.getposts);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Color(0xffBC32FD),
      ),
      drawer: const DrawerScreen(),
      body: Container(
        padding: const EdgeInsets.all(16),
        color: Colors.white,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text(
                  "Let's Find Job in EU",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
                ),
                Icon(
                  Icons.notifications,
                  color: Color(0xffBC32FD),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
                //height: 50,
                //width: double.infinity,
                //padding: EdgeInsets.all(5),
                child: TextField(
                    onSubmitted: (value) {
                      Get.to(Search(), arguments: value);
                    },
                    cursorColor: Color(0xffBC32FD),
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xffBC32FD), width: 2),
                        ),
                        hintText: "Search jobs or position",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffBC32FD))),
                        prefixIcon: Padding(
                            padding: EdgeInsets.all(0),
                            child: Icon(Icons.search))))),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(width: 1, color: Colors.grey)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Get.to(Filter(), arguments: "Full-time");
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.lock_clock,
                          size: 40,
                          color: Color(0xffBC32FD),
                        ),
                        Text("fulltime")
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.to(Filter(), arguments: "experienced");
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.punch_clock,
                          size: 40,
                          color: Color(0xffBC32FD),
                        ),
                        Text("experienced")
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.to(Filter(), arguments: "mid");
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.exposure,
                          size: 40,
                          color: Color(0xffBC32FD),
                        ),
                        Text("mid-level")
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Get.to(Filter(), arguments: "entry");
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.newspaper,
                          size: 40,
                          color: Color(0xffBC32FD),
                        ),
                        Text("entry")
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: const Text(
                "Jobs For You",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(child: Obx(() {
              return controller.postloading.value
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      //shrinkWrap: true,
                      //scrollDirection: Axis.vertical,
                      itemCount: 20,
                      itemBuilder: (context, index) {
                        var item = controller.getposts[index];
                        return GestureDetector(
                            onTap: () {
                              print("success ${index}");
                              Get.to(Details(), arguments: item);
                            },
                            child: Container(
                              margin: EdgeInsets.only(bottom: 10),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(width: 1, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    item.title,
                                    style: TextStyle(fontSize: 20),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                          child: Text(
                                        item.company_name,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                      Text("loc: ${item.location}"),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                          child: Container(
                                              child: Text("types",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold)))),
                                      Expanded(
                                          child: Text('tags',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)))
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Flexible(
                                          child: Container(
                                        child: ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: item.job_types.length,
                                            itemBuilder: (c, i) {
                                              var a = item.job_types;
                                              return Container(
                                                child:
                                                    item.job_types.length == 0
                                                        ? Text("-")
                                                        : Text(a[i]),
                                              );
                                            }),
                                      )),
                                      Flexible(
                                          child: Container(
                                        child: ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: item.tags.length,
                                            itemBuilder: (c, i) {
                                              var a = item.tags;
                                              return Container(
                                                child: Text(
                                                  a[i],
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              );
                                            }),
                                      ))
                                    ],
                                  ),
                                ],
                              ),
                            ));
                      });
            })
                // Column(
                //   children: [

                //   ],
                // ),
                ),
          ],
        ),
      ),
    );
  }
}
