import 'dart:async';

import 'package:camera/camera.dart';
import 'package:eujobs/components/drawer.dart';
import 'package:eujobs/components/takepic.dart';
import 'package:eujobs/pages/main/Account.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:eujobs/routes/index.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final TextEditingController _nameController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser!.displayName;

  @override
  void initState() {
    super.initState();
    setState(() {
      user;
    });
  }

  // void _navRef(BuildContext context) async {
  //   final result = await Get.to(() => TakePictureScreen());
  //   if (result != null) {
  //     _navRef; // call your own function here to refresh screen
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffBC32FD),
          title: const Text('Edit Profile'),
        ),
        //drawer: const DrawerScreen(),
        body: Container(
            padding: EdgeInsets.all(10),
            child: ListView(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 150,
                      height: 150,
                      child: Stack(
                        children: <Widget>[
                          Align(
                              alignment: Alignment.bottomRight,
                              child: GestureDetector(
                                onTap: () => Get.to(() => TakePictureScreen()),
                                child: Icon(Icons.edit),
                              )),
                          Container(
                            width: 150,
                            height: 150,
                            child: CircleAvatar(
                                //radius: 50,
                                backgroundImage: auth.currentUser!.photoURL ==
                                        null
                                    ? AssetImage('lib/assets/ava.jpg')
                                    : NetworkImage(auth.currentUser!.photoURL!)
                                        as ImageProvider),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Divider(),
                    Text('Display Name:'),
                    Divider(),
                    Container(
                        //padding: EdgeInsets.all(10),
                        child: TextField(
                      cursorColor: Color(0xffBC32FD),
                      controller: _nameController,
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xffBC32FD), width: 2),
                          ),
                          border: OutlineInputBorder(),
                          labelText: "Name",
                          floatingLabelStyle:
                              TextStyle(color: Color(0xffBC32FD))),
                    )),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: ElevatedButton(
                      onPressed: () async {
                        await auth.currentUser!
                            .updateDisplayName(_nameController.text);
                        Get.to(Home());
                      },
                      child: Text("save profile"),
                      style: raisedButtonStyle,
                    )),
              ],
            )));
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  //textStyle: TextStyle(color: Colors.white),
  primary: Color(0xffBC32FD),
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
