import 'package:eujobs/components/drawer.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:eujobs/pages/main/EditProfile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Account extends StatelessWidget {
  const Account({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    //print(auth.currentUser!.photoURL);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xffBC32FD),
          title: const Text('Account'),
        ),
        drawer: const DrawerScreen(),
        body: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 20,
                ),
                CircleAvatar(
                    radius: 50,
                    backgroundImage: auth.currentUser!.photoURL == null
                        ? AssetImage('lib/assets/ava.jpg')
                        : NetworkImage(auth.currentUser!.photoURL!)
                            as ImageProvider),
                SizedBox(
                  height: 20,
                ),
                Divider(),
                Text('Display Name:'),
                Divider(),
                auth.currentUser!.displayName != null
                    ? Text(
                        "${auth.currentUser!.displayName}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      )
                    : Text(
                        "${auth.currentUser!.email}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                SizedBox(
                  height: 10,
                ),
                Divider(),
                Text('Email:'),
                Divider(),
                Text(
                  auth.currentUser!.email!,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ],
            ),
            Container(
                margin: EdgeInsets.only(bottom: 10),
                child: ElevatedButton(
                  style: raisedButtonStyle,
                  onPressed: () => Get.to(EditProfile()),
                  child: Text("edit profile"),
                )),
          ],
        )));
  }
}

final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
  onPrimary: Colors.grey[300],
  //textStyle: TextStyle(color: Colors.white),
  primary: Color(0xffBC32FD),
  minimumSize: Size(88, 36),
  padding: EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(6)),
  ),
);
