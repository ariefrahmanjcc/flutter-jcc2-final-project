import 'dart:async';
import 'package:eujobs/model/model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Services {
  Future<List<Data>?> getallposts() async {
    try {
      var response = await http
          .get(Uri.parse("https://arbeitnow.com/api/job-board-api"))
          .timeout(const Duration(seconds: 10), onTimeout: () {
        throw TimeoutException("connection timeout. try again");
      });
      if (response.statusCode == 200) {
        Map jsonresponse = convert.jsonDecode(response.body);
        List jsonData = jsonresponse['data'];
        return jsonData.map((e) => new Data.fromJson(e)).toList();
      } else {
        return null;
      }
    } on TimeoutException catch (_) {
      print("respone timeout");
    }
  }
}
