import 'package:eujobs/model/model.dart';
import 'package:eujobs/services/services.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  var getposts = <Data>[].obs;
  //print('getpost $getposts');
  Services services = Services();
  var postloading = true.obs;
  @override
  void onInit() {
    callpostmethod();
    super.onInit();
  }

  callpostmethod() async {
    try {
      postloading.value = true;
      var result = await services.getallposts();
      //print('result $result');
      if (result != null) {
        getposts.assignAll(result);
      } else {
        print("null");
      }
    } finally {
      postloading.value = false;
    }
    update();
  }
}
