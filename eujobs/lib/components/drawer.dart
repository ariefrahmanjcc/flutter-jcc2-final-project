import 'package:eujobs/pages/main/EditProfile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  Future<void> _signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
    } catch (e) {
      print(e); // TODO: show dialog with error
    }
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser!.email);
    }
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
            decoration: BoxDecoration(
              color: Color(0xffBC32FD),
            ),
            accountName: auth.currentUser!.displayName != null
                ? Text("${auth.currentUser!.displayName}")
                : Text("${auth.currentUser!.email}"),
            currentAccountPicture: CircleAvatar(
                backgroundImage: auth.currentUser!.photoURL == null
                    ? AssetImage('lib/assets/ava.jpg')
                    : NetworkImage(auth.currentUser!.photoURL!)
                        as ImageProvider),
            accountEmail: Text("${auth.currentUser!.email}")),
        DrawerListTile(
          iconData: Icons.group,
          title: "Edit Profile",
          onTilePressed: () {
            Get.to(() => EditProfile());
          },
        ),
        const Divider(),
        DrawerListTile(
            iconData: Icons.logout, title: "Logout", onTilePressed: _signOut)
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: const TextStyle(fontSize: 16),
      ),
    );
  }
}
