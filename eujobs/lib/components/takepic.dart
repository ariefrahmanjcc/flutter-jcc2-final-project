import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:eujobs/main.dart';
import 'package:eujobs/pages/main/Account.dart';
import 'package:eujobs/pages/main/EditProfile.dart';
import 'package:eujobs/routes/index.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen({
    Key? key,
    //required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;
  late CameraDescription camera;
  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      cameras[0],
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0xffBC32FD),
          title: const Text('Take a picture')),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return CameraPreview(_controller);
          } else {
            // Otherwise, display a loading indicator.
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffBC32FD),
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await _initializeControllerFuture;

            // Attempt to take a picture and get the file `image`
            // where it was saved.
            final image = await _controller.takePicture();

            // If the picture was taken, display it on a new screen.
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(
                    // Pass the automatically generated path to
                    // the DisplayPictureScreen widget.
                    imagePath: image.path,
                    imageName: image.name),
              ),
            );
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final String imageName;
  Future<void> uploadFile(String filePath) async {
    File file = File(filePath);
    FirebaseAuth auth = FirebaseAuth.instance;
    //FirebaseStorage storage = FirebaseStorage.instance;
    Reference ref = FirebaseStorage.instance.ref('uploads/${imageName}');
    try {
      await ref.putFile(file);
      String imageUrl = await ref.getDownloadURL();
      print(imageUrl);
      auth.currentUser!.updatePhotoURL(imageUrl);
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
    }
  }

  const DisplayPictureScreen(
      {Key? key, required this.imagePath, required this.imageName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    //print(imagePath);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Display the Picture'),
        backgroundColor: Color(0xffBC32FD),
      ),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xffBC32FD),
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await uploadFile(imagePath);

            // Attempt to take a picture and get the file `image`
            // where it was saved.
            //final image = await _controller.takePicture();

            // If the picture was taken, display it on a new screen.
            Get.to(() => Home());
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
        child: const Icon(Icons.upload),
      ),
    );
  }
}
