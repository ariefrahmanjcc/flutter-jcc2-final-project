import 'dart:convert';

List<Data> postmodelFromJson(String str) =>
    List<Data>.from(json.decode(str).map((x) => Data.fromJson(x)));
String postmodelToJson(List<Data> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Jobs {
  List<Data> data;
  Jobs({required this.data});

  factory Jobs.fromJson(Map<String, dynamic> json) {
    var list = json['data'] as List;
    print(list.runtimeType);
    List<Data> dataList = list.map((i) => Data.fromJson(i)).toList();
    return Jobs(data: dataList);
  }
}

class Data {
  Data({
    required this.slug,
    required this.company_name,
    required this.title,
    required this.description,
    required this.remote,
    required this.url,
    required this.tags,
    required this.job_types,
    required this.location,
    required this.created_at,
  });

  String slug;
  String company_name;
  String title;
  String description;
  bool remote;
  String url;
  List<dynamic> tags;
  List<dynamic> job_types;
  String location;
  int created_at;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
      slug: json['slug'],
      company_name: json['company_name'],
      title: json['title'],
      description: json['description'],
      remote: json['remote'],
      url: json['url'],
      tags: json['tags'],
      job_types: json['job_types'],
      location: json['location'],
      created_at: json['created_at']);

  Map<String, dynamic> toJson() => {
        "slug": slug,
        "company_name": company_name,
        "title": title,
        "description": description,
        "remote": remote,
        "url": url,
        "tags": tags,
        "job_types": job_types,
        "location": location,
        "created_at": created_at
      };
}
