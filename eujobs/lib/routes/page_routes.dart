import 'package:eujobs/components/takepic.dart';
import 'package:eujobs/pages/auth/LoginScreen.dart';
import 'package:eujobs/pages/auth/RegisterScreen.dart';
import 'package:eujobs/pages/auth/SplashScreen.dart';
import 'package:eujobs/pages/main/Account.dart';
import 'package:eujobs/pages/main/Detail.dart';
import 'package:eujobs/pages/main/EditProfile.dart';
import 'package:eujobs/pages/main/Filter.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:eujobs/pages/main/Search.dart';
import 'package:eujobs/routes/index.dart';
import 'package:get/get.dart';
import 'route_name.dart';

class AuthRoutesApp {
  static final pages = [
    GetPage(name: AuthRouteName.Splash, page: () => Splash()),
    GetPage(name: AuthRouteName.Login, page: () => Login()),
    GetPage(name: AuthRouteName.Register, page: () => Register()),
  ];
}

class MainRoutesApp {
  static final pages = [
    GetPage(name: MainRouteName.Home, page: () => Home()),
    GetPage(name: MainRouteName.Detail, page: () => Details()),
    GetPage(name: MainRouteName.Search, page: () => Search()),
    GetPage(name: MainRouteName.Filter, page: () => Filter()),
    GetPage(name: MainRouteName.EditProfile, page: () => EditProfile()),
    GetPage(name: MainRouteName.Account, page: () => Account()),
    GetPage(name: MainRouteName.takePic, page: () => TakePictureScreen())
  ];
}
