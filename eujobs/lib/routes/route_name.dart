import 'package:eujobs/pages/main/Account.dart';

abstract class AuthRouteName {
  static const Login = '/login';
  static const Register = '/register';
  static const Splash = '/splash';
}

abstract class MainRouteName {
  static const Home = '/home';
  static const Detail = '/detail';
  static const Account = '/account';
  static const EditProfile = '/editprofile';
  static const Search = '/search';
  static const Filter = '/filter';
  static const takePic = '/takepic';
}
