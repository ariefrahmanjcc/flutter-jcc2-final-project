import 'package:eujobs/controller/controller.dart';
import 'package:eujobs/pages/main/Account.dart';
import 'package:eujobs/pages/main/Detail.dart';
import 'package:eujobs/pages/main/Dashboard.dart';
import 'package:eujobs/pages/main/Search.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
//import 'dart:html';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  State<Home> createState() => _HomeScreen();
}

class _HomeScreen extends State<Home> {
  late List<Widget> _pages;
  late Widget _page1;
  late Widget _page2;
  late int _currentIndex;
  late Widget _currentPage;
  @override
  void initState() {
    super.initState();
    _page1 = const Dashboard();
    _page2 = const Account();
    _pages = [_page1, _page2];
    _currentIndex = 0;
    _currentPage = _page1;
  }

  void _changeTab(int index) {
    setState(() {
      _currentIndex = index;
      _currentPage = _pages[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _currentPage,
      //drawer: const DrawerScreen(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.search),
          //   label: 'Search',
          // ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Account',
          ),
        ],
        currentIndex: _currentIndex,
        selectedItemColor: Color(0xffBC32FD),
        onTap: (index) {
          _changeTab(index);
        },
      ),
    );
  }
}
